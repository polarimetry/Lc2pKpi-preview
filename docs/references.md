# Bibliography

:::{only} html

```{button-link} _static/references.bib
:color: secondary
:shadow:
Download BibTeX
```

:::

```{bibliography} /_static/references.bib
---
style: unsrt_et_al
cited:
---
```

$~$ <!-- activate math mode -->
